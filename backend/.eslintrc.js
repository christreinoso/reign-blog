module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    tsconfigRootDir: __dirname,
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint/eslint-plugin'],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  rules: {
    "no-useless-constructor": 0,
    "no-empty-function": ["error", { "allow": ["constructors"] }],
    "max-len": ["error", { "code": 120, "ignoreComments": true, "ignoreUrls": true, "ignoreStrings": true }],
    "no-inline-comments": "error",
    "no-unused-vars": 0,
    "@typescript-eslint/no-unused-vars": 2,
    "linebreak-style": 0,
    "import/no-unresolved": [
      0,
      {
        "caseSensitive": false
      }
    ],
    "indent": [
      1,
      2,
      {
        "MemberExpression": 0
      }
    ], 
    "import/prefer-default-export": "off",
    "no-trailing-spaces": 0, 
    "space-before-blocks": [
      1,
      "always"
    ], 
    "lines-between-class-members": [
      1,
      "always"
    ],
    "padding-line-between-statements": [
      "warn",
      {
        "blankLine": "always",
        "prev": "*",
        "next": "export"
      }, 
      {
        "blankLine": "any",
        "prev": "import",
        "next": "import"
      } 
    ],
    "no-duplicate-imports": [
      "warn",
      {
        "includeExports": true
      }
    ],
    "prettier/prettier": ["error", {
     "endOfLine":"auto"
    }],
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off'
  },
};
