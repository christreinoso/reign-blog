export const environment = {
  production: false,
  postedArticlesEndpoint:
    'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
};
