/* eslint-disable prettier/prettier */
import { HttpService, Injectable, OnModuleInit } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Article } from './interfaces/article.interface';
import { CreateArticleDTO } from './dto/article.dto';
import { interval, Subscription } from 'rxjs';

@Injectable()
export class ArticleService implements OnModuleInit {
  BLOG_URL = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

  /**
   *
   */
  source = interval(3600000);

  /**
   *
   */
  subscription: Subscription = new Subscription();

  constructor(
    private readonly httpService: HttpService,
    @InjectModel('Article') private readonly articleModel: Model<Article>,
  ) {}

  onModuleInit() {
    this.subscription = this.source.subscribe(() => {
      this.getArticlesFromAPI();
    });
  }

  async getArticlesFromAPI() {
    this.getArticlesFromService().then((res) => {
      this.createArticleInDatabase(res).then(() => {
        console.log('Database filled with articles');
      });
    });
  }

  async getArticlesFromService(): Promise<CreateArticleDTO> {
    return await (await this.httpService.get(this.BLOG_URL).toPromise()).data;
  }

  async getArticlesFromDatabase(): Promise<Article[]> {
    const articles = await this.articleModel.find();
    return articles;
  }

  async getLatestArticlesFromDatabase(): Promise<Article> {
    const article = await this.articleModel
    .findOne()
    .sort({ field: 'asc', _id: -1 })
    .limit(1);
    return article;
  }

  async createArticleInDatabase(createArticleDTO: CreateArticleDTO): Promise<Article> {
    return await new this.articleModel(createArticleDTO).save();
  }

  async updateArticleInDatabase(
    articleID: number,
    createArticleDTO: CreateArticleDTO,
  ): Promise<Article> {
    const updatedArticle = await this.articleModel.findByIdAndUpdate(
      articleID,
      createArticleDTO,
      { new: true },
    );
    return updatedArticle;
  }
}
