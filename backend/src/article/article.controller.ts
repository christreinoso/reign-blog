/* eslint-disable indent */
import {
  Body,
  Controller,
  Get,
  HttpStatus,
  NotFoundException,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { ArticleService } from './article.service';
import { CreateArticleDTO } from './dto/article.dto';

@Controller('article')
export class ArticleController {
  constructor(private articleService: ArticleService) {}

  /**
   * Fill database with articles at first time
   * @param res
   */
  @Get('/initData')
  async firstArticleDataFill(@Res() res) {
    await this.articleService
      .getArticlesFromService()
      .then(async (articleDTO) => {
        await this.articleService
          .createArticleInDatabase(articleDTO)
          .then((article) => {
            res.status(HttpStatus.OK).json({
              message: 'Init data processed',
              article,
            });
          });
      })
      .catch((err) => {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          message: err,
        });
      });
  }

  /**
   * Get articles from database
   * @param res
   */
  @Get('/')
  async getAllArticleDocuments(@Res() res) {
    await this.articleService
      .getArticlesFromDatabase()
      .then((articles) => {
        res.status(HttpStatus.OK).json({
          message: 'All Articles',
          articles: articles,
        });
      })
      .catch((err) => {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          message: err,
        });
      });
  }

  /**
   * Get latest article from database
   * @param res
   */
  @Get('/latest')
  async getLatestArticleDocument(@Res() res) {
    await this.articleService
      .getLatestArticlesFromDatabase()
      .then((article) => {
        res.status(HttpStatus.OK).json({
          message: 'Latest Article',
          article,
        });
      })
      .catch((err) => {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          message: err,
        });
      });
  }

  /**
   * Update article
   * @param res
   * @param createArticleDTO
   * @param articleID
   */
  @Put('/update')
  async updateArticleDocument(
    @Res() res,
    @Body() createArticleDTO: CreateArticleDTO,
    @Query('articleID') articleID,
  ) {
    const article = await this.articleService.updateArticleInDatabase(
      articleID,
      createArticleDTO,
    );
    if (!article) throw new NotFoundException('Article does not exist!');
    return res.status(HttpStatus.OK).json({
      message: 'Article Updated Successfully',
      article,
    });
  }
}
