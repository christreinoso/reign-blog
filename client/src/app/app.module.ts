import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticleListComponent } from './components/article-list/article-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ChronologicalDatePipe } from './pipe/date-format-pipe';

@NgModule({
  declarations: [AppComponent, ArticleListComponent, ChronologicalDatePipe],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
