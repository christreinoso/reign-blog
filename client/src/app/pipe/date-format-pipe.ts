import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'ChronologicalDatePipe' })
export class ChronologicalDatePipe implements PipeTransform {
  transform(date: Date | string) {
    const dateRecieved = new Date(date);
    const utcDate = convertToUTCDate(dateRecieved);

    const timeDiffInDays = Math.floor(
      (new Date().getTime() - utcDate.getTime()) / 86400000
    );
    const datePipe = new DatePipe('en-US');

    if (timeDiffInDays < 1) {
      return datePipe.transform(utcDate, 'h:mm a');
    }
    if (timeDiffInDays == 1) {
      return 'Yesterday';
    }
    return datePipe.transform(utcDate, 'MMM d');
  }
}

function convertToUTCDate(dateRecieved: Date): Date {
  const time = dateRecieved.getTime();

  if (dateRecieved.getTimezoneOffset() > 0) {
    const final = time + Math.abs(dateRecieved.getTimezoneOffset() * 60000);
    return new Date(final);
  } else {
    const final = time + -Math.abs(dateRecieved.getTimezoneOffset() * 60000);
    return new Date(final);
  }
}
