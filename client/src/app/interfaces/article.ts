export interface Article {
  _id?: string;
  created_at: Date;
  hits: [
    {
      _id?: string;
      created_at: Date;
      title: string;
      url: string;
      author: string;
      points: number;
      story_text: string;
      comment_text: string;
      num_comments: number;
      story_id: number;
      story_title: string;
      story_url: string;
      parent_id: number;
      created_at_i: number;
      _tags: [string];
      objectID: string;
      _highlightResult: {
        author: {
          value: string;
          matchLevel: string;
          matchedWords: [string];
        };
        comment_text: {
          value: string;
          matchLevel: string;
          fullyHighlighted: boolean;
          matchedWords: [string];
        };
        story_title: {
          value: string;
          matchLevel: string;
          matchedWords: [string];
        };
      };
    }
  ];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  query: string;
  params: string;
  processingTimeMS: number;
}
