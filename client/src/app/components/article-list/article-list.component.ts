/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable indent */
/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/article';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {
  /**
   *
   */
  postedArticle_!: Article;

  /**
   *
   */
  article = {} as Article;

  constructor(private articleService: ArticleService) {}

  ngOnInit(): void {
    this.getLatestArticle();
  }

  getLatestArticle() {
    this.articleService.getLatestArticleFromService().subscribe(
      (res) => {
        this.article = res.article;
        if (this.article === null) return;
        this.article.hits.forEach((hitElement, index) => {
          if (hitElement.story_title === null && hitElement.title === null) {
            this.article.hits.splice(index, 1);
          }
        });
      },
      (err) => console.error(err)
    );
  }

  updateArticleData(_id: string) {
    this.articleService.getLatestArticleFromService().subscribe(
      (res) => {
        this.article = res.article;
        this.article.hits.forEach((hitElement, index) => {
          if (hitElement._id === _id) {
            this.article.hits.splice(index, 1);
            return;
          }
        });
        this.articleService
          .updateArticleFromService(this.article._id || '', this.article)
          .subscribe(
            (res) => {
              this.article = res.article;
            },
            (err) => console.log(err)
          );
      },
      (err) => console.error(err)
    );
  }
}
