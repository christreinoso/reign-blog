/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ArticleService } from 'src/app/services/article.service';

import { ArticleListComponent } from './article-list.component';

describe('ArticleListComponent', () => {
  let component: ArticleListComponent;
  let fixture: ComponentFixture<ArticleListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ArticleListComponent],
      providers: [
        {
          provide: ArticleListComponent,
          useValue: {
            getLatestArticle: () => ({}),
            updateArticleData: () => ({})
          }
        },
        {
          provide: ArticleService,
          useValue: {
            getLatestArticleFromService: () =>
              of({
                article: {
                  _id: '',
                  hits: []
                }
              }),
            updateArticleFromService: () => of({})
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
