/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../interfaces/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  BASE_URL = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  getLatestArticleFromService(): Observable<any> {
    return this.http.get<any>(`${this.BASE_URL}/article/latest`);
  }

  updateArticleFromService(id: string, article: Article): Observable<any> {
    return this.http.put<any>(
      `${this.BASE_URL}/article/update?articleID=${id}`,
      article
    );
  }
}
