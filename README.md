# Tech Stack

Backend

- NodeJs v14.15.0 
- NestJS 7.5.4

Database

- MongoDB v4.4.2

Frontend

- Angular CLI: 11.0.5

# Testing - Code coverage

Go to backend folder and execute

- npm test

Go to client folder and execute

- npm test --codeCoverage=true

# Gitlab - CI

The CI/CD configuration files **.gitlab-ci.yml** for the Backend and Frontend (client) artefacts, are located at the root folder of each component, and they pass the next Pipelines Stages.

  - install
  - test
  - build
  - deploy

# Docker - Compose

Go to root folder and execute

- docker-compose build

That creates the containers for the backend, frontend (client) and MongoDB images, after that start the containers with

- docker-compose up

After containers are up, they are listen on the next ports

- **Backend:** 3000 (http://localhost:3000/)

- **Frontend:** 4200 (http://localhost:4200/) 

- **MongoDB:** 27017, The name of the Database collection is Reign-Blog

# App Starting

As a pre requisite use Postman to populate the database at first time, so make a GET request to the next Endpoint.

- http://localhost:3000/article/initData

After the Endpoint returns 200 Ok status code with message **Init data processed**, open the next URL to open the Blog page.

- http://localhost:4200/

You can interact with the app since here.

As requiered the Backend component get data from the Angolia Endpoint each hour.
